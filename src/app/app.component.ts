import { Component } from '@angular/core';
import * as XLSX from 'xlsx';
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent {
  title = 'app works!';

  // Example code from https://github.com/SheetJS/js-xlsx/issues/526
  // Thank you: https://github.com/junaidinam

  saveExcel(param: any, filename: string) {
      var wb = new Workbook();

      var write = new Array;
      param.forEach(function (row,index) {

          var each = new Array;
          var keys = Object.keys(row); // all the keys
          if (index == 0) {
              // column headers
              for (var i = 0; i < keys.length; i++) {
                  each.push(keys[i]);
              }
              write.push(each); // write header
              each = [];
              for (var i = 0; i < keys.length; i++) {
                  each.push(row[keys[i]]);
              }
          }
          else
          {
              for (var i = 0; i < keys.length; i++) {
                  each.push(row[keys[i]]);
              }
          }
          write.push(each);
      }, this);

      var data = write;

      var ws_name = "Sheet 1";
      wb.SheetNames.push(ws_name);
      wb.Sheets[ws_name] = this.sheet_from_array_of_arrays(data);

      var wbout = XLSX.write(wb, { bookType: 'xlsx', type: 'binary' }); //bookSST: true,
      saveAs(new Blob([this.s2ab(wbout)], { type: "application/octet-stream" }), filename+".xlsx");
  }


  s2ab(s) {
      var buf = new ArrayBuffer(s.length);
      var view = new Uint8Array(buf);
      for (var i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
      return buf;
  }

  sheet_from_array_of_arrays(data: any, opts?: any): any {
      var ws: any = {};

      var wscols = [
          { wch: 20 },
          { wch: 20 },
          { wch: 20 },
          { wch: 20 },
          { wch: 20 },
          { wch: 20 },
          { wch: 20 },
          { wch: 20 },
          { wch: 20 },
          { wch: 20 },
          { wch: 20 }
      ];

      var startCell = { c: 10000000, r: 10000000 };
      var endCell = { c: 0, r: 0 };

      var range = { s: startCell, e: endCell };
      for (var R = 0; R != data.length; ++R) {
          for (var C = 0; C != data[R].length; ++C) {
              if (range.s.r > R) range.s.r = R;
              if (range.s.c > C) range.s.c = C;
              if (range.e.r < R) range.e.r = R;
              if (range.e.c < C) range.e.c = C;
              //var cell = { v: data[R][C], t: 'n' };
              //if (cell.v == null) continue;
              //var cell_ref = XLSX.utils.encode_cell({ c: C, r: R });

              //if (typeof cell.v === 'number') cell.t = 'n';
              //else if (typeof cell.v === 'boolean') cell.t = 'b';
              //else cell.t = 's';

              //var cell = new Cell();
              var cell: any = {};
              cell.v = data[R][C];
              //console.log(cell);
              var cell_ref = XLSX.utils.encode_cell({ c: C, r: R });
              //console.log(cell_ref);
              if (cell.v == null) continue;
              if (typeof cell.v === 'number') cell.t = 'n';
              else if (typeof cell.v === 'boolean') cell.t = 'b';
              else cell.t = 's';
              //console.log(cell);
              ws[cell_ref] = cell;
              //console.log(ws);
          }
      }
      if (range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(startCell, endCell);
      ws['!cols'] = wscols;
      return ws;
  }

  clicked(event) {
    // var data = [
    //     [1, 2, 3],
    //     [true, false, null, "sheetjs"],
    //     ["foo", "bar", new Date("2014-02-19T14:30Z"), "0.3"],
    //     ["baz", null, "qux"]
    //   ];

    var data = [
      {
        columnOne: 'one',
        columnTwo: 'two'
      },
      {
        columnOne: 'three',
        columnTwo: 4
      },
      {
        columnOne: new Date(),
        columnTwo: null
      }
    ];

    this.saveExcel(data, "test-file");

    // let blob = new Blob(['Hello Blob!'], { type: 'text/plain;charset=utf-8' });
    // saveAs(blob, 'hey-blob.txt');

    console.log(`I was clicked - ${event}`);
  };

}

export class Workbook {
SheetNames: any = [];
Sheets: any = {};
}

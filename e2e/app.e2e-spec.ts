import { TestXlsxPage } from './app.po';

describe('test-xlsx App', () => {
  let page: TestXlsxPage;

  beforeEach(() => {
    page = new TestXlsxPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
